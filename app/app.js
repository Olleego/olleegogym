/**
 * Main app module.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
  'use strict';

  angular.module('app', [
    // angular modules
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'ngMaterial',
    'ngResource',
    'ngSanitize',
    'ngProgress',
    'lfNgMdFileInput',
    'md.data.table',
    // 3rd party modules
    'ui.router',
    'restangular',
    'LocalStorageModule',
    // app modules
    'app.core',
    'app.signin',
    'app.reset',
    'app.dashboard',
    'app.header',
    'app.member'
  ]);

})();
