$(function(){


	//알림 & 설정 레이어
	/*
		var $layer_group = $('.layer_group > div');
		var $layer_bt = $('.layer_bt');
		var $report = $('#header .report');
		var $report_layer = $('#header .report_layer');
		var $setting = $('#header .setting');
		var $setting_layer = $('#header .setting_layer');

		$report.click(function(){
			if(!$report_layer.is('.active')) hd_layer_in($(this), $report_layer);
			else hd_layer_out($(this), $report_layer);

			if($report_layer.is('.active')) hd_layer_out($setting, $setting_layer);
		});

		$setting.click(function(){
			if(!$setting_layer.is('.active')) hd_layer_in($(this), $setting_layer);
			else hd_layer_out($(this), $setting_layer);

			if($report_layer.is('.active')) hd_layer_out($report, $report_layer);
		});

		function hd_layer_in(bt_this, target){
			bt_this.addClass('on');
			target.css('z-index',11).siblings().css('z-index',10);
			target.addClass('active').show().stop().animate({'marginLeft':-365}, {duration:400, easing:'easeOutCubic'});
		}

		function hd_layer_out(bt_this, target){
			bt_this.removeClass('on');
			target.removeClass('active').stop().animate({'marginLeft':0}, {duration:400, easing:'easeInOutCubic'}).fadeOut(0);
		}
	*/


		var $layer_group = $('.layer_group > div');
		var $layer_bt = $('.lnb .layer_bt');
		var hd_layer_w = [];

		$layer_group.each(function(i, e){
			var wid = $(e).outerWidth()+25;
			hd_layer_w.push(wid);
		});

		$layer_bt.click(function(){
			var idx = $(this).parent().index();
			if( $layer_group.eq(idx).is('.active') ) hd_layer_out(idx);
			else hd_layer(idx);
		});
		function hd_layer(idx){
			var speed = 400;
			if( idx == 1 ) speed = 550;
			$layer_bt.parent().eq(idx).toggleClass('on').siblings().removeClass('on');
			$layer_group.eq(idx).css('z-index',11).siblings().css('z-index',10);
			$layer_group.eq(idx).addClass('active').show().stop().animate({'marginLeft':-hd_layer_w[idx]}, {duration:speed, easing:'easeOutCubic'});
			$layer_group.eq(idx).siblings().removeClass('active').stop().animate({'marginLeft':0}, {duration:speed, easing:'easeInOutCubic'}).fadeOut(0);
		}
		function hd_layer_out(idx){
			var speed = 400;
			if( idx == 1 ) speed = 550;
			$layer_bt.parent().eq(idx).removeClass('on');
			$layer_group.removeClass('active').stop().animate({'marginLeft':0}, {duration:speed, easing:'easeInOutCubic'}).fadeOut(0);
		}








		//레이어 이외의 영역 클릭 시 닫힘(필요시 주석 해제)
		/*
		$layer_group.on({
			mouseenter:function(){
				$(this).removeClass('over');
			},
			mouseleave:function(){
				$(this).addClass('over');
			}
		});
		$('body, html').click(function(){
		});
		*/


	//트레이너별 관리회원
		var $trainer_mb = $('.ma_trainer_mb .lst li');
		var trainer_length = $trainer_mb.length;
		$trainer_mb.each(function(i, e){
			$(e).find('.chart > p').each(function(){
				var num = $(this).find('span').text();
				//$(this).find('i').animate({'width':num}, 500); //애니메이션 적용
				$(this).find('i').css({'width':num});
			});
		});
		$('.ma_trainer_mb .total span').text(trainer_length);

		$('.ma_trainer_mb .lst').bxSlider({
			mode:'vertical', //fade
			auto:true,
			minSlides: 4,
			pause:4000,
			pager:true,
			controls:true,
			pagerType:'short'
		});


	//PT권 판매통계
		var $pt_stats = $('.ma_pt_stats .tab li');
		var $pt_chart = $('.ma_pt_stats .con > div');
		$pt_stats.eq(0).addClass('on');
		$pt_chart.not(':first').hide();
		$pt_stats.click(function(){
			var idx = $(this).index();
			$(this).addClass('on').siblings().removeClass('on');
			$pt_chart.eq(idx).show().siblings().hide();
		});


	//성별 분포도
		var $sex_chart = $('.sex_chart > ul');
		var sex_man = $sex_chart.find('.man span').text();
		var sex_wom = $sex_chart.find('.woman span').text();
		$sex_chart.next().find('span:first').text(sex_man).next().text(sex_wom);
		$sex_chart.find('li:first').css('height',sex_man).next().css('height',sex_wom);


	//연령별 분포도
		var $age_diagram = $('.ma_age_diagram dl');
		$age_diagram.each(function(i, e){
			$(e).find('p').each(function(){
				var num = $(this).find('.percent').text();
				$(this).find('.bar').css({'width':num});
				$(this).find('.percent').css({'left':num});
			});
		});


	//메인 예약 스케줄
		var $reserv_layer = $('.reserv_layer');
		var $reserv_layerHd = $reserv_layer.find('.lst_hd');
		var $reserv_layerLi = $reserv_layer.find('.lst_bd > ul');
		$reserv_layerHd.find('li.on').append('<i class="line"><i></i><i></i></i>');
		var hd_onidx = $reserv_layerHd.find('li.on').index();
		$reserv_layerLi.each(function(i, e){
			var arr = [];
			$(e).find('li').eq(hd_onidx).addClass('on').append('<i class="line"><i></i><i></i><i></i></i>');
			$(e).find('li').each(function(){
				arr.push($(this).innerHeight()-12);
			});
			arr.sort(function(a,b){return b-a});
		    $(e).find('li').css({'height':arr[0], 'padding':'12px 0 2px'});
		});
		$reserv_layer.find('.lst_bd > ul:last li').eq(hd_onidx).addClass('bd_last');




});