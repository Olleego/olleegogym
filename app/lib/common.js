$(function(){


	//네비 활성화
		var page_tit = $('#header h2').text();
		var hd_gnb = $('#header .gnb li');
		var hd_gnb_t = [];
		hd_gnb.each(function(i, e){
			var gt = $(e).find('a').text();
			hd_gnb_t.push(gt);
		});
		for( i=0; i < hd_gnb.length; i++ ){
			if( page_tit == hd_gnb_t[i] ){
				hd_gnb.eq(i).addClass('on').siblings().removeClass('on');
			}
		}


	//알림 & 설정 & 예약 레이어
		var $layer_group = $('.layer_group > div');
		var $layer_bt = $('.lnb .layer_bt');
		var hd_layer_w = [];

		$layer_group.each(function(i, e){
			var wid = $(e).outerWidth()+25;
			hd_layer_w.push(wid);
		});

		$layer_bt.click(function(){
			var idx = $(this).parent().index();
			if( $layer_group.eq(idx).is('.active') ) hd_layer_out(idx);
			else hd_layer(idx);
			return false;
		});

		function hd_layer(idx){
			var speed = 350;
			if( idx == 1 ) speed = 450;
			$layer_bt.parent().eq(idx).toggleClass('on').siblings().removeClass('on');
			$layer_group.eq(idx).css('z-index',11).siblings().css('z-index',10);
			$layer_group.eq(idx).addClass('active').show().stop().animate({'marginLeft':-hd_layer_w[idx]}, {duration:speed, easing:'easeOutCubic'});
			$layer_group.eq(idx).siblings().removeClass('active').stop().animate({'marginLeft':0}, {duration:speed, easing:'easeInOutCubic'}).fadeOut(0);
		}
		function hd_layer_out(idx){
			var speed = 350;
			if( idx == 1 ) speed = 450;
			$layer_bt.parent().eq(idx).removeClass('on');
			$layer_group.removeClass('active').stop().animate({'marginLeft':0}, {duration:speed, easing:'easeInOutCubic'}).fadeOut(0);
		}

		//레이어 밖 화면 클릭 시 닫힘
		var ly_out = false;
		$layer_group.each(function(i, e){
			$(e).on({
				mouseenter:function(){
					ly_out = true;
				},
				mouseleave:function(){
					ly_out = false;
				}
			});
		});
		$('html').click(function(){
			if( $layer_bt.parent().is('.on') && !ly_out ){
				var lidx = $layer_bt.parent().siblings('li.on').index();
				hd_layer_out(lidx);
			}
		});

		//설정 레이어 인풋 placeholder
		$(window).load(function(){
			$('.t_up li').each(function(i, e){
				if( !$(e).find('input').val() == '' ){
					$(e).find('p').css('background-position','0 -9999px');
				}
			});
		});
		$('.t_up input').on({
			keydown:function(){
				$(this).parent().css('background-position','0 -9999px');
			},
			focusout:function(){
				if( !$(this).val() == '' ){
					$(this).parent().css('background-position','0 -9999px');
				}else{
					$(this).parent().css('background-position','0 0');
				}
			}
		});


	//예약 스케줄 챠트
		var $reserv_layer = $('.reserv_layer');
		var $reserv_layerHd = $reserv_layer.find('.lst_hd');
		var $reserv_layerLi = $reserv_layer.find('.lst_bd > ul');
		$reserv_layerHd.find('li.on').append('<i class="line"><i></i><i></i></i>');
		var hd_onidx = $reserv_layerHd.find('li.on').index();
		$reserv_layerLi.each(function(i, e){
			var arr = [];
			$(e).find('li').eq(hd_onidx).addClass('on').append('<i class="line"><i></i><i></i><i></i></i>');
			$(e).find('li').each(function(){
				arr.push($(this).innerHeight()-30);
			});
			arr.sort(function(a,b){return b-a});
		    $(e).find('li').css({'height':arr[0], 'padding':'12px 0 18px'});
		});
		$reserv_layer.find('.lst_bd > ul:last li').eq(hd_onidx).addClass('bd_last');



	//트레이너별 관리회원
		var $trainer_mb = $('.ma_trainer_mb .lst li');
		var trainer_length = $trainer_mb.length;
		$trainer_mb.each(function(i, e){
			console.log('i ' , i);
			$(e).find('.chart > p').each(function(){
				var num = $(this).find('span').text();
				$(this).find('i').animate({'width':num}, 500); //애니메이션 적용
				//$(this).find('i').css({'width':num});
			});
		});
		$('.ma_trainer_mb .total span').text(trainer_length);
		console.log('test' );
		$('.ma_trainer_mb .lst').bxSlider({
			mode:'vertical', //fade
			auto:true,
			minSlides: 4,
			pause:4000,
			pager:true,
			controls:true,
			pagerType:'short'
		});

	//PT권 판매통계
		var $pt_stats = $('.ma_pt_stats .tab li');
		var $pt_chart = $('.ma_pt_stats .con > div');
		$pt_stats.eq(0).addClass('on');
		$pt_chart.not(':first').hide();
		$pt_stats.click(function(){
			var idx = $(this).index();
			$(this).addClass('on').siblings().removeClass('on');
			$pt_chart.eq(idx).show().siblings().hide();
		});


	//성별 분포도
		var $sex_chart = $('.sex_chart > ul');
		var sex_man = $sex_chart.find('.man span').text();
		var sex_wom = $sex_chart.find('.woman span').text();
		$sex_chart.next().find('span:first').text(sex_man).next().text(sex_wom);
		$sex_chart.find('li:first').css('height',sex_man).next().css('height',sex_wom);


	//연령별 분포도
		var $age_diagram = $('.ma_age_diagram dl');
		$age_diagram.each(function(i, e){
			$(e).find('p').each(function(){
				var num = $(this).find('.percent').text();
				$(this).find('.bar').css({'width':num});
				$(this).find('.percent').css({'left':num});
			});
		});


	//기본 텝 메뉴
		/*
		var $tab_tab = $('.tab_box');
		$tab_tab.each(function(i, e){
			var tab_a = $(e).find('> .tab > li');
			var tab_c = $(e).find('> .tcon > div');
			tab_a.eq(0).addClass('on');
			tab_c.not(':first').hide();
			tab_a.click(function(){
				var idx = $(this).index();
				$(this).addClass('on').siblings().removeClass('on');
				tab_c.eq(idx).show().siblings().hide();
			});
		});
		*/


	//데이트피커
		var datepk = $(".datepk");
		datepk.each(function(i, e){
			var datepk_h = $(e).height();
			$(e).wrap('<span class="datepk_wrap">');
			$(e).parent().css('height',datepk_h+2)
			$(e).datepicker({
				  showOn: "button",
				  buttonImage: "../_img/datepk_btn.png",
				  buttonImageOnly: true,
				  buttonText: "Select date"
			});
			$(e).next().wrap('<span class="datepk_btn">');
			$(e).click(function(){
				$(e).next().children().click();
			});
		});



});