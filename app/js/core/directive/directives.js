var myDir = angular.module('olleego.directives', []);

myDir.directive('pwdmatch', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var pwd1elem = angular.element(document.getElementById(attrs.pwdmatch));
            function isMatch() {
                scope.$apply(function () {
                    ctrl.$setValidity('pwdmatch', elem.val() === pwd1elem.val());
                });
            }

            elem.on('keyup', isMatch);
            pwd1elem.on('keyup', isMatch);
        }
    }
});

myDir.directive('pastcheck', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            ctrl.$validators.pastcheck = function(modelValue, viewValue) {

                if(modelValue && (modelValue instanceof Date)) {
                    var now = new Date();
                    console.log("m:"+modelValue);
                    console.log("n:"+now);
                    console.log("m:"+modelValue.getTime());
                    console.log("n:"+now.getTime());

                    if(modelValue.getTime() < now.getTime()) {
                        // it is invalid
                        return false;
                    }else {
                        // it is valid
                        return true;
                    }
                }else {
                    // it is invalid
                    return false;
                }
            };
        }
    }
});

myDir.directive('eventFocus', function(focus) {
    return function(scope, elem, attr) {
        elem.on(attr.eventFocus, function() {
            focus(attr.eventFocusId);
        });

        // Removes bound events in the element itself
        // when the scope is destroyed
        scope.$on('$destroy', function() {
            elem.off(attr.eventFocus);
        });
    };
});

myDir.directive('preventDrag', function ($ionicGesture, $ionicSlideBoxDelegate) {
    return {
        restrict: 'A',
        link    : function (scope, elem) {
            var reportEvent = function (e) {
                if (e.target.tagName.toLowerCase() === 'input') {
                    $ionicSlideBoxDelegate.enableSlide(false);
                } else {
                    $ionicSlideBoxDelegate.enableSlide(true);
                }
            };
            $ionicGesture.on('touch', reportEvent, elem);
        }
    };
});


