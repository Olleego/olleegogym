/**
 * Signin route.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name signinRoute
     * @module app.signin
     * @requires $stateProvider
     * @description
     * Router for the signin page.
     *
     * @ngInject
     */
    function dashboardRoute($stateProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                views:{
                    'header':{
                        templateUrl: 'js/routes/header/header.html',
                        controller: 'HeaderCtrl as vm',
                        resolve:{
                            center:  function(CenterService,$rootScope){
                                console.log('$rootScope.me._id ' , $rootScope.me);
                                return CenterService.get($rootScope.me._id);
                            }
                        }
                    },
                    'container':{
                        templateUrl: 'js/routes/dashboard/dashboard.html',
                        controller: 'DashBoardCtrl as vm'
                    }
                },
                data:{
                    authenticate : true
                }
            });
    }
    angular
        .module('app.dashboard')
        .config(dashboardRoute);

})();
