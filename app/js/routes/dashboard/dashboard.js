/**
 * Signin module.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.signin
     */

    var app =  angular.module('app.dashboard', []);
    /*app.directive('ptChart',function(){
        return{
            restrict:"AE",
            scope:{
                data:"@",
                name:"@",
                num:"@",
                comp:"@",
                reserve:"@"
            },
            templateUrl:"js/routes/dashboard/ptchart.html",
            link:function(scope,element,attrs){
                console.log("ptchart " , scope.data);
                console.log("ptchart " , scope.name);
                console.log("ptchart " , scope.num);
                console.log("ptchart " , scope.comp);
                console.log('여기도 호출');
                var chart;
                var phase = scope.$$phase;

                console.log('i++', i++);

               var fn = function(){
                       chart = AmCharts.makeChart("ptchart"+scope.data, {
                            "type": "serial",
                            "categoryField":"feild",
                            "categoryAxis":{
                                "autoWrap":false
                            },
                            "theme": "light",
                            "rotate": true,
                            "startDuration": 1,
                            "trendLines": [],
                            "graphs": [
                                {
                                    "balloonText": "회원수:[[value]]",
                                    "fillAlphas": 10,
                                    "id": "AmGraph-1",

                                    "title": "회원수",
                                    "type": "column",
                                    "valueField": "num"
                                },
                                {
                                    "balloonText": "PT 완료:[[value]]",
                                    "fillAlphas": 10,
                                    "id": "AmGraph-2",

                                    "title": "PT 완료",
                                    "type": "column",
                                    "valueField": "comp"
                                },
                                {
                                    "balloonText": "PT 예약:[[value]]",
                                    "fillAlphas": 10,
                                    "id": "AmGraph-3",

                                    "title": "PT 예약",
                                    "type": "column",
                                    "valueField": "reserve"
                                }
                            ],
                            "guides": [],
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "position": "bottom",
                                    "axisAlpha": 0,
                                    "offset":1,
                                    "labelsEnabled": true
                                }
                            ],
                            "allLabels": [],
                            "balloon": {},
                            "titles": [],
                            "dataProvider": [
                                {
                                    "feild":"",
                                    "num": scope.num,
                                    "comp": scope.comp,
                                    "reserve": scope.reserve
                                }
                            ],
                            "export": {
                                "enabled": true
                            }

                        });

                    };
                    fn();
                chart.validateData();
            }
        }
    });
  */
    app.directive('bxSlider', ['$timeout',function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //element.bxSlider(scope.$eval('{' + attrs.bxSlider + '}'));
                scope.$on('repeatFinished', function () {
                    console.log("ngRepeat has finished");
                    element.bxSlider(scope.$eval('{' + attrs.bxSlider + '}'));
                });
            }
        }
    }]).directive('notifyWhenRepeatFinished', ['$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    console.log('scope ' , scope);
                    if (scope.$last === true) {
                        scope.$emit('repeatFinished');
                    }
                }
            }
        }]);

})();
