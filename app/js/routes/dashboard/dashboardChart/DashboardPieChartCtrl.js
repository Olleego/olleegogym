/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('app.dashboard')
      .controller('DashboardPieChartCtrl', DashboardPieChartCtrl);

  /** @ngInject */
  function DashboardPieChartCtrl($scope, $timeout) {
    
    $scope.charts = [{
      color: "#11111",
      description: 'New Visits',
      stats: '57,820',
      icon: 'person',
      image:"img/trainer_mb_thum_sample.jpg"
    }, {
      color: "#11111",
      description: 'Purchases',
      stats: '$ 89,745',
      icon: 'money',
      image:"img/trainer_mb_thum_sample.jpg"
    }, {
      color: "#11111",
      description: 'Active Users',
      stats: '178,391',
      icon: 'face',
      image:"img/trainer_mb_thum_sample.jpg"
    }, {
      color: "#11111",
      description: 'Returned',
      stats: '32,592',
      icon: 'refresh',
      image:"img/trainer_mb_thum_sample.jpg"
    }, {
      color: "#11111",
      description: 'Returned',
      stats: '32,592',
      icon: 'refresh',
      image:"img/trainer_mb_thum_sample.jpg"
    }, {
      color: "#11111",
      description: 'Returned',
      stats: '32,592',
      icon: 'refresh',
      image:"img/trainer_mb_thum_sample.jpg"
    }, {
      color: "#11111",
      description: 'Returned',
      stats: '32,592',
      icon: 'refresh',
      image:"img/trainer_mb_thum_sample.jpg"
    }
    ];

    function getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }

    function loadPieCharts() {
      $('.chart').each(function () {
        var chart = $(this);
        console.log("chart " , chart[0].id);
        console.log(chart[0].id !="");
        if(chart[0].id !=""){
          AmCharts.makeChart(""+chart[0].id, {
            "type": "serial",
            "categoryField":"feild",
            "categoryAxis":{
              "autoWrap":false
            },
            "theme": "light",
            "rotate": true,
            "startDuration": 1,
            "trendLines": [],
            "graphs": [
              {
                "balloonText": "회원수:[[value]]",
                "fillAlphas": 10,
                "id": "AmGraph-1",

                "title": "회원수",
                "type": "column",
                "valueField": "num"
              },
              {
                "balloonText": "PT 완료:[[value]]",
                "fillAlphas": 10,
                "id": "AmGraph-2",

                "title": "PT 완료",
                "type": "column",
                "valueField": "comp"
              },
              {
                "balloonText": "PT 예약:[[value]]",
                "fillAlphas": 10,
                "id": "AmGraph-3",

                "title": "PT 예약",
                "type": "column",
                "valueField": "reserve"
              }
            ],
            "guides": [],
            "valueAxes": [
              {
                "id": "ValueAxis-1",
                "position": "bottom",
                "axisAlpha": 0,
                "offset":1,
                "labelsEnabled": true
              }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
              {
                "feild":"",
                "num": 1,
                "comp": 2,
                "reserve": 3
              }
            ],
            "export": {
              "enabled": true
            }

          });
        }
 /*       var amchart =
        amchart.validateData();*/
       /* chart.easyPieChart({
          easing: 'easeOutBounce',
          onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          },
          barColor: chart.attr('rel'),
          trackColor: 'rgba(0,0,0,0)',
          size: 84,
          scaleLength: 0,
          animation: 2000,
          lineWidth: 9,
          lineCap: 'round',
        });*/
      });

      $('.refresh-data').on('click', function () {
        updatePieCharts();
      });
    }

    function updatePieCharts() {
      /*$('.pie-charts .chart').each(function(index, chart) {
        console.log('cahrt ' , chart);
        console.log('$(chart).data',$(chart).data('easyPieChart'));
        $(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
      });*/
    }
//class="lst" data-bx-slider="mode: 'vertical',auto:true,pagerType:'short',pause:4000, pager: true, controls: true, minSlides:1, slideWidth: 350, slideMargin:10, infiniteLoop: true, hideControlOnEnd: true"
    $timeout(function () {
      loadPieCharts();
      updatePieCharts();
      $(".bxslider").bxSlider({
        mode:'vertical', //fade
        auto:false,
        minSlides: 1,
        maxSlides: 6,
        pause:4000,
        pager:true,
        controls:true,
        pagerType:'short'
      })
    }, 1000);
  }
})();