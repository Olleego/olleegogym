/**
 * Signin controller.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc controller
     * @name SigninCtrl
     * @module app.signin
     * @requires $rootScope
     * @requires $state
     * @requires Authentication
     * @requires $cordovaVibration
     * @description
     * Controller for the signin page.
     *
     * @ngInject
     */


    function DashBoardCtrl($scope){
        var vm = this;
        var items=[];
        console.log('호출');
        vm.comp = 100;
        vm.reserve = 200;
        vm.cancel = 300;
        vm.pt_stats = {
            data1:200,
            data2:300,
            data3:400
        };
        vm.ma_membership = 400;
         items = [
         {
             name:"홍길동0",
             num:20,
             comp:30,
             reserve:10
         },
         {
             name:"홍길동1",
             num:20,
             comp:30,
             reserve:10
         },
         {
             name:"홍길동2",
             num:20,
             comp:30,
             reserve:10
         },
         {
             name:"홍길동3",
             num:20,
             comp:30,
             reserve:10
         },
         {
             name:"홍길동4",
             num:20,
             comp:30,
             reserve:10
         },
         {
             name:"홍길동5",
             num:20,
             comp:30,
             reserve:10
         },
         {
             name:"홍길동6",
             num:20,
             comp:30,
             reserve:10
         }

     ];
        $scope.items = items;
        vm.total = items.length;
     var chart= AmCharts.makeChart("chartdiv", {
           "type": "serial",
           "categoryField": "date",
           "dataDateFormat": "YYYY-MM-DD",
           "colors": [
               "#29b6f6",
               "#ffc107",
               "#B0DE09",
               "#0D8ECF",
               "#2A0CD0",
               "#CD0D74",
               "#CC0000",
               "#00CC00",
               "#0000CC",
               "#DDDDDD",
               "#999999",
               "#333333",
               "#990000"
           ],
           "categoryAxis": {
               "boldPeriodBeginning": false,
               "color":"#888",
               "dateFormats": [
                   {
                       "period": "fff",
                       "format": "JJ:NN:SS"
                   },
                   {
                       "period": "ss",
                       "format": "JJ:NN:SS"
                   },
                   {
                       "period": "mm",
                       "format": "JJ:NN"
                   },
                   {
                       "period": "hh",
                       "format": "JJ:NN"
                   },
                   {
                       "period": "DD",
                       "format": "DD일"
                   },
                   {
                       "period": "WW",
                       "format": "MMM DD"
                   },
                   {
                       "period": "MM",
                       "format": "MM월"
                   },
                   {
                       "period": "YYYY",
                       "format": "YYYY년"
                   }
               ],
               "markPeriodChange": false,
               "minPeriod": "DD", //일
               "parseDates": true,
               "axisAlpha": 0,
               "axisThickness": 0,
               "gridCount": 7,
               "gridThickness": 0,
               "minHorizontalGap": 20,
               "showFirstLabel": false,
               "showLastLabel": false,
               "tickLength": 0,
               "titleBold": false,
               "titleRotation": 0
           },
           "trendLines": [],
           "graphs": [
               {
                   "bullet": "round",
                   "bulletSize": 10,
                   "id": "AmGraph-1",
                   "lineThickness": 2,
                   "title": "graph 1",
                   "valueField": "column-1"
               },
               {
                   "bullet": "round",
                   "bulletSize": 10,
                   "id": "AmGraph-2",
                   "lineThickness": 2,
                   "title": "graph 2",
                   "valueField": "column-2"
               }
           ],
           "guides": [],
           "valueAxes": [
               {
                   "id": "ValueAxis-1",
                   "axisThickness": 0,
                   "labelsEnabled": false,
                   "minVerticalGap": 37,
                   "showFirstLabel": false,
                   "showLastLabel": false,
                   "tickLength": 0,
                   "title": ""
               }
           ],
           "allLabels": [],
           "balloon": {
               "borderThickness": 0,
               "fillAlpha": 0
           },
           "titles": [
               {
                   "id": "Title-1",
                   "size": 15,
                   "text": ""
               }
           ]
       });
        setDataSet('a');
        $scope.setDataSet =  setDataSet;
        function setDataSet(sel) {
            var data1 =  [
                {
                    "date": "2014-03-01",
                    "column-1": 8,
                    "column-2": 5
                },
                {
                    "date": "2014-03-02",
                    "column-1": 6,
                    "column-2": 0
                },
                {
                    "date": "2014-03-03",
                    "column-1": 2,
                    "column-2": 3
                },
                {
                    "date": "2014-03-04",
                    "column-1": 1,
                    "column-2": 3
                },
                {
                    "date": "2014-03-05",
                    "column-1": 2,
                    "column-2": 1
                },
                {
                    "date": "2014-03-06",
                    "column-1": 3,
                    "column-2": 2
                },
                {
                    "date": "2014-03-07",
                    "column-1": 6,
                    "column-2": 8
                }
            ];
            var data = [{
                    "date": "2014-03-01",
                    "column-1": 3,
                    "column-2": 5
                },
                {
                    "date": "2014-03-02",
                    "column-1": 1,
                    "column-2": 3
                },
                {
                    "date": "2014-03-03",
                    "column-1": 2,
                    "column-2": 5
                },
                {
                    "date": "2014-03-04",
                    "column-1": 2,
                    "column-2": 3
                },
                {
                    "date": "2014-03-05",
                    "column-1": 2,
                    "column-2": 4
                },
                {
                    "date": "2014-03-06",
                    "column-1": 1,
                    "column-2": 2
                },
                {
                    "date": "2014-03-07",
                    "column-1": 6,
                    "column-2": 8
                }];
            if(sel =="y"){
                chart.dataProvider = data;
                chart.validateData();
            }else{
                chart.dataProvider = data1;
                chart.validateData();
            }
        }
    }

    angular
        .module('app.dashboard')
        .controller('DashBoardCtrl', DashBoardCtrl);


})();


