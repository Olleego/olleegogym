/**
 * Signin controller.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc controller
     * @name SigninCtrl
     * @module app.signin
     * @requires $rootScope
     * @requires $state
     * @requires Authentication
     * @requires $cordovaVibration
     * @description
     * Controller for the signin page.
     *
     * @ngInject
     */
    function SigninCtrl($scope,$timeout, ngProgressFactory, $state,Authentication) {
        $scope.progressbar = ngProgressFactory.createInstance();

        var vm = this;
        vm.disable = false;
        vm.submit = function( isValid,sig) {
            $scope.progressbar.start();
            if(!isValid) { return; }
            vm.disable = true;
            Authentication.signin(sig)
                .then(function(r){
                    $scope.progressbar.complete();
                    vm.disable = false;
                    $state.go('dashboard')
                })
                .catch(function(e){
                    $scope.progressbar.complete();
                    console.log('e' ,e);
                });

/*            $timeout(function(){
                vm.disable = false;
                console.log('test');
            },1000);*/

         };
        vm.goToSignup = function(){
            $state.go('join_step01');
        };
        vm.goReset = function(){
            $state.go('reset');
        };
    }

    angular
        .module('app.signin')
        .controller('SigninCtrl', SigninCtrl);
})();
