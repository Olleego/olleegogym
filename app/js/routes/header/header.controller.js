/**
 * Signin controller.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc controller
     * @name HeaderCtrl
     * @module app.header
     * @requires $rootScope
     * @requires $state
     * @requires Authentication
     * @description
     * Controller for the header page.
     *
     * @ngInject
     */
    function HeaderCtrl($rootScope,$scope,  $state,center) {
        var vm = this;
        vm.center = center.result;
        console.log('vm.center ' ,vm.center.result);
        vm.go = function(d){
            $state.go(d);
        };

        vm.disable = false;
        vm.alarm = [];
        vm.alarm.push({ date:"1", con:"3" });
        vm.alarm.push({ date:"1", con:"3" });
        vm.alarm.push({ date:"1", con:"3" });
        vm.alarm.push({ date:"1", con:"3" });
        vm.alarm.push({ date:"1", con:"3" });
        //네비 활성화
        var page_tit = $('#header h2').text();
        var hd_gnb = $('#header .gnb li');
        var hd_gnb_t = [];
        hd_gnb.each(function(i, e){
            var gt = $(e).find('a').text();
            hd_gnb_t.push(gt);
        });
        for(var i=0; i < hd_gnb.length; i++ ){
            if( page_tit == hd_gnb_t[i] ){
                hd_gnb.eq(i).addClass('on').siblings().removeClass('on');
            }
        }


        //알림 & 설정 & 예약 레이어
        var $layer_group = $('.layer_group > div');
        var $layer_bt = $('.lnb .layer_bt');
        var hd_layer_w = [];

        $layer_group.each(function(i, e){
            var wid = $(e).outerWidth()+25;
            hd_layer_w.push(wid);
        });

        vm.alClick = function(){

            var idx = $(this).parent().index();
            console.log('idx ' , idx);
            if( $layer_group.eq(idx).is('.active') ) hd_layer_out(idx);
            else hd_layer(idx);
            return false;
        };
        /*$layer_bt.click(function(){
            vm.test = [];
            vm.test.push({ date:"1", con:"3" });
            vm.test.push({ date:"1", con:"3" });
            vm.test.push({ date:"1", con:"3" });
            vm.test.push({ date:"1", con:"3" });
            vm.test.push({ date:"1", con:"3" });
            $scope.$apply();
            /!*vm.test = [{ date:"1", con:"3" },
                { date:"2", con:"2" },
                { date:"3", con:"1" },
                { date:"4", con:"1" }];*!/
            setTimeout(function(){

            },200);
            var idx = $(this).parent().index();
            if( $layer_group.eq(idx).is('.active') ) hd_layer_out(idx);
            else hd_layer(idx);
            return false;
        });*/


        //레이어 밖 화면 클릭 시 닫힘
        var ly_out = false;
        $layer_group.each(function(i, e){
            $(e).on({
                mouseenter:function(){
                    ly_out = true;
                },
                mouseleave:function(){
                    ly_out = false;
                }
            });
        });
        $('html').click(function(){
            if( $layer_bt.parent().is('.on') && !ly_out ){
                var lidx = $layer_bt.parent().siblings('li.on').index();
                hd_layer_out(lidx);
            }
        });

        //설정 레이어 인풋 placeholder
        $(window).load(function(){
            $('.t_up li').each(function(i, e){
                if( !$(e).find('input').val() == '' ){
                    $(e).find('p').css('background-position','0 -9999px');
                }
            });
        });
        $('.t_up input').on({
            keydown:function(){
                $(this).parent().css('background-position','0 -9999px');
            },
            focusout:function(){
                if( !$(this).val() == '' ){
                    $(this).parent().css('background-position','0 -9999px');
                }else{
                    $(this).parent().css('background-position','0 0');
                }
            }
        });


        //예약 스케줄 챠트
        var $reserv_layer = $('.reserv_layer');
        var $reserv_layerHd = $reserv_layer.find('.lst_hd');
        var $reserv_layerLi = $reserv_layer.find('.lst_bd > ul');
        $reserv_layerHd.find('li.on').append('<i class="line"><i></i><i></i></i>');
        var hd_onidx = $reserv_layerHd.find('li.on').index();
        $reserv_layerLi.each(function(i, e){
            var arr = [];
            $(e).find('li').eq(hd_onidx).addClass('on').append('<i class="line"><i></i><i></i><i></i></i>');
            $(e).find('li').each(function(){
                arr.push($(this).innerHeight()-30);
            });
            arr.sort(function(a,b){return b-a});
            $(e).find('li').css({'height':arr[0], 'padding':'12px 0 18px'});
        });
        $reserv_layer.find('.lst_bd > ul:last li').eq(hd_onidx).addClass('bd_last');


        

       



        function hd_layer(idx){
            var speed = 350;
            if( idx == 1 ) speed = 450;
            $layer_bt.parent().eq(idx).toggleClass('on').siblings().removeClass('on');
            $layer_group.eq(idx).css('z-index',11).siblings().css('z-index',10);
            $layer_group.eq(idx).addClass('active').show().stop().animate({'marginLeft':-hd_layer_w[idx]}, {duration:speed, easing:'easeOutCubic'});
            $layer_group.eq(idx).siblings().removeClass('active').stop().animate({'marginLeft':0}, {duration:speed, easing:'easeInOutCubic'}).fadeOut(0);
        }
        function hd_layer_out(idx){
            var speed = 350;
            if( idx == 1 ) speed = 450;
            $layer_bt.parent().eq(idx).removeClass('on');
            $layer_group.removeClass('active').stop().animate({'marginLeft':0}, {duration:speed, easing:'easeInOutCubic'}).fadeOut(0);
        }

    }


    angular
        .module('app.dashboard')
        .controller('HeaderCtrl', HeaderCtrl);
})();


