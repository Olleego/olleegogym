/**
 * Signin controller.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc controller
     * @name MemberCtrl
     * @module app.member
     * @requires $rootScope
     * @requires $state
     * @requires Authentication
     * @description
     * Controller for the member page.
     *
     * @ngInject
     */
    function MemberCtrl($rootScope,$scope,  $state) {
        var vm = this;
        //트레이너 카운트
        var trainer_lst = $('.trainer');
        var trainer_num = trainer_lst.find('dd').length;
        trainer_lst.find('>dt i').text(trainer_num);
        trainer_lst.find('a').click(function(){
            $(this).addClass('on').parent().siblings().find('a').removeClass('on');
        });

        vm.detail = function(){
            console.log('test');
            $state.go("member.detail");
        }
    }

    function MemberDetailCtrl($rootScope,$scope,  $state) {
        var vm = this;
        //트레이너 카운트
        var trainer_lst = $('.trainer');
        var trainer_num = trainer_lst.find('dd').length;
        trainer_lst.find('>dt i').text(trainer_num);
        trainer_lst.find('a').click(function(){
            $(this).addClass('on').parent().siblings().find('a').removeClass('on');
        });



    }


    angular
        .module('app.member')
        .controller('MemberCtrl', MemberCtrl)
        .controller('MemberDetailCtrl', MemberDetailCtrl);
})();


