/**
 * Signin route.
 *
 * @author    Martin Micunda {@link http://martinmicunda.com}
 * @copyright Copyright (c) 2015, Martin Micunda
 * @license   The MIT License {@link http://opensource.org/licenses/MIT}
 */
(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name memberRoute
     * @module app.member
     * @requires $stateProvider
     * @description
     * Router for the member page.
     *
     * @ngInject
     */
    function memberRoute($stateProvider) {
        $stateProvider
            .state('members', {
                url:'/members',
                abstract: true,
                views:{
                    'header':{
                        templateUrl: 'js/routes/header/header.html',
                        controller: 'HeaderCtrl as vm'
                    },
                    'container':{
                        templateUrl: '<div ui-view=""></div>'
                    }
                },
                resolve: {
                    UserService: 'UserService'
                },
                data:{
                    authenticate : false
                }
            })
            .state("members.list", {
                url: '/members/list',
                templateUrl: 'js/routes/member/member.list.html',
                controller: 'MemberCtrl as vm',
                data:{
                    authenticate : false
                }
            })
            .state("members.detail", {
                url: '/members/detail',
                templateUrl: 'js/routes/member/member.detail.html',
                controller: 'MemberDetailCtrl as vm',
                data:{
                    authenticate : false
                }
            });
            /*.state('detail', {
                url: '/member/detail',
                views:{
                    'header':{
                        templateUrl: 'js/routes/header/header.html',
                        controller: 'HeaderCtrl as vm'
                    },
                    'container':{
                        templateUrl: 'js/routes/member/member.detail.html',
                        controller: 'MemberDetailCtrl as vm'
                    }
                },
                data:{
                    authenticate : false
                }
            })*/
    }
    /*angular
        .module('app.member')
        .config(memberRoute);*/

})();
