
(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name resetRoute
     * @module app.reset
     * @requires $stateProvider
     * @description
     * Router for the reset page.
     *
     * @ngInject
     */
    function resetRoute($stateProvider) {
        $stateProvider
            .state('reset', {
                url: '/reset',
                templateUrl: 'js/routes/reset/reset.html',
                controller: 'ResetCtrl as vm'
            });
    }

    angular
        .module('app.reset')
        .config(resetRoute);

})();
