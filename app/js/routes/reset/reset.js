
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name app.reset
     */
    angular.module('app.reset', []);

})();
