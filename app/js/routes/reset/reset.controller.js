(function () {
    'use strict';

    /**
     * @ngdoc controller
     * @name ResetCtrl
     * @module app.reset
     * @requires $rootScope
     * @requires $state
     * @requires $ionicPopup
     * @requires serverUtil
     * @description
     * Controller for the reset page.
     *
     * @ngInject
     */
    function ResetCtrl($scope,$rootScope,$ionicPopup,  $state,  Authentication,consts,ngProgressFactory) {
        var vm = this;
        vm.disable = false;
        var title = "";
        vm.reset = function(credentials, isValid) {
            vm.disable = true;
            $rootScope.progressbar.start();
            console.log('test'  ,isValid);
            console.log('credentials.email ' , credentials.email);
            if(!isValid) { return; }
            Authentication.reset(consts.NODE_API_URL+"/forgot",{email:credentials.email,type:"json"})
                .then(function(result){
                    $rootScope.progressbar.complete();
                    console.log('test 11');
                    if(result.data.success){
                        title = "성공";
                    }else{
                        title = "실패";
                    }
                    $ionicPopup.alert({
                        title: title,
                        template: result.data.msg
                    }).then(function(){
                            if(result.data.success){
                                setTimeout(
                                    function(){
                                        $state.go("signin");
                                    },500
                                )
                            }else{
                                vm.disable = false;
                            }
                        });
                },function(err){
                    $rootScope.progressbar.complete();
                    console.log('test');
                    $ionicPopup.alert({
                        title: '실패',
                        template: '이메일 발송 실패'
                    }).then(
                        function(){
                            console.log('err ' , err);
                            vm.disable = false;
                        }
                    );
                });
        };
    }

    angular
        .module('app.reset')
        .controller('ResetCtrl', ResetCtrl);
})();
